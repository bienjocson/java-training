package fishExercise;

import java.util.Arrays;

class Seafood implements cookSeafood{
	int size;
	String name;
	
	final int minFishCookTypes=0;
	final int grilledCookTypeIndex=2;
	final int maxCrustaceanCookTypes=5;
	
	final int minFishIndex = 0;
	final int maxFishIndex = 2;
	final int minCrustaceanIndex = 3;
	final int maxCrustaceanIndex = 5;

	int nameIndex;
	
	String[] seafoodName = Fisherman.seafoodName;
	String[] cookTypeName = Fisherman.cookTypeName;
	
	public void get()	{
		System.out.println("You are holding a raw " + size + " gram " + name); 
	}
	public String getName()
	{
		return name;
	}
	public int getSize()
	{
		return size;
	}
	public String getSeafoodType() {
		nameIndex=Arrays.asList(seafoodName).indexOf(name);
		String type="";
		if(nameIndex>=minFishIndex && nameIndex<=maxFishIndex)
			type="Fish";
		else
			type="Crustaceans";
		
		return type;
	}
	public void cook(int cookType)	{
		nameIndex=Arrays.asList(seafoodName).indexOf(name);
		if((cookType>=minFishCookTypes && grilledCookTypeIndex<=2) && (nameIndex>=minFishIndex && nameIndex<=maxFishIndex) ||
			(cookType>=grilledCookTypeIndex && cookType<=maxCrustaceanCookTypes) && (nameIndex>=minCrustaceanIndex && nameIndex<=maxCrustaceanIndex))
			System.out.println("You ate a " + cookTypeName[cookType] + " " + name + " that weighed " + size + " grams when it was caught."); 	
		else
			System.out.println("You ate a Spoiled " + name + " that weighed " + size + " grams when it was caught.");
	}
}
