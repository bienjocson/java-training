package fishExercise;

public class Anchovy extends Fish {
	public Anchovy()
	{
		super.minimumSize = 1;
		super.maximumSize = 50;
		super.name="Anchovy";
		super.size=generateSize(minimumSize,maximumSize);
	}
	
}
