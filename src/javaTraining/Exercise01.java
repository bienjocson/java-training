package javaTraining;
interface Component
{
	void changeBrand(String newValue);
	void changeModel(String newValue);
}
interface Processor {

    void changeCores(int newValue);
    void changeThreads(int newValue);
    void changeTDP(int newValue);
    void changeBaseFrequency(double newValue);
    void maxFrequency(double increment);
}

interface StorageDrive {
    void changeMemorySize(int newValue);
    void changeCacheSize(int newValue);
    void changeRPM(int newValue);
}

class CPU implements Processor,Component {
	String model;
	String brand;
	int cores;
    int threads;
    int tdp;
    double frequency;
    double max_frequency;

    public void changeBrand(String newValue) {
    	brand = newValue;
   }
    public void changeModel(String newValue) {
    	model = newValue;
   }
    public void changeCores(int newValue) {
    	cores = newValue;
   }
    public void changeThreads(int newValue) {
    	threads = newValue;
   }
    public void changeTDP(int newValue) {
    	tdp = newValue;
   }
    public void changeBaseFrequency(double newValue) {
    	frequency = newValue;
   }
    public void maxFrequency(double increment) {
    	max_frequency= frequency+increment;
   }
    void printComponent() {
        System.out.println("Component: CPU" +
            " Brand: " + brand +
            " Model: " + model +
            " Cores: " + cores +
            " Threads: " + threads +
            " TDP: " + tdp +
            " Frequency: " + frequency + " - " + max_frequency
    		);
   }
}

class DiskDrive implements StorageDrive,Component {
	String model;
	String brand;
	int memorysize;
    int cachesize;
    int rpm;

    public void changeBrand(String newValue) {
    	brand = newValue;
   }
    public void changeModel(String newValue) {
    	model = newValue;
   }
    public void changeMemorySize(int newValue) {
    	memorysize = newValue;
   }
    public void changeCacheSize(int newValue) {
    	cachesize = newValue;
   }
    public void changeRPM(int newValue) {
    	rpm = newValue;
   }
    void printComponent() {
        System.out.println("Component: HDD" +
            " Brand: " + brand +
            " Model: " + model +
            " Memory Size: " + memorysize +
            " Cache Size: " + cachesize +
            " Revolutions Per Minute: " + rpm
    		);
   }
}
class SSD extends DiskDrive {
	double form_factor;
	void printComponent() {
        System.out.println("Component: SSD" +
            " Brand: " + brand +
            " Model: " + model +
            " Memory Size: " + memorysize +
            " Cache Size: " + cachesize +
            " Revolutions Per Minute: " + rpm +
            " Form Factor: " + form_factor
    		);
   }
}
public class Exercise01 {

	public static void main(String[] args) {
		// Create two different 
        // CPU objects
		CPU i3 = new CPU();
        CPU ryzen3 = new CPU();
        
        //I3 CPU OBJECT
        i3.changeBrand("Intel");
        i3.changeModel("I3-10100");
        
        
        i3.changeCores(4);
        i3.changeThreads(8);
        i3.changeTDP(65);
        i3.changeBaseFrequency(3.6);
        
        i3.maxFrequency(0.7);
        
        
      //RYZEN3 CPU OBJECT
        ryzen3.changeBrand("AMD");
        ryzen3.changeModel("Ryzen 3 3100");
        
        
        ryzen3.changeCores(4);
        ryzen3.changeThreads(8);
        ryzen3.changeTDP(65);
        ryzen3.changeBaseFrequency(3.6);
        

        
        ryzen3.maxFrequency(0.3);
        
        //Create a
        //HDD object
        DiskDrive WDCaviar = new DiskDrive();
		//WDCaviar HDD OBJECT
		WDCaviar.changeBrand("Western Digital");
        WDCaviar.changeModel("Caviar Blue");
        
        WDCaviar.changeMemorySize(2000);
        WDCaviar.changeCacheSize(64);
        WDCaviar.changeRPM(5400);
        
        //Create an
        //SSD object
        SSD CrucialBX = new SSD();
		//Crucial SSD OBJECT
        CrucialBX.changeBrand("Crucial");
        CrucialBX.changeModel("BX500");
        
        CrucialBX.changeMemorySize(240);
        CrucialBX.changeCacheSize(0);
        CrucialBX.changeRPM(10000);
        CrucialBX.form_factor = 2.5;
        
        i3.printComponent();
        ryzen3.printComponent();
        WDCaviar.printComponent();
        CrucialBX.printComponent();

	}

}
