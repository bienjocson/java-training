package fishExercise;

public class Tilapia extends Fish {
	public Tilapia()	{
		super.minimumSize = 51;
		super.maximumSize = 200;
		super.name="Tilapia";
		super.size=generateSize(minimumSize,maximumSize);
	}
}
