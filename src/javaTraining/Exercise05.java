package javaTraining;
import java.util.Scanner; 
import java.lang.Math; 

public class Exercise05 {
	static Scanner scanner = new Scanner(System.in);
	static double getMortgage(double principal, double rate, double period) {
		double monthlyRate = (rate/100)/12;
		double numberOfPayments = period*12;
		return principal*
				((monthlyRate*Math.pow(1+monthlyRate,numberOfPayments))
				/(Math.pow(1+monthlyRate,numberOfPayments)-1));
	}
	static double readNumber(double min, double max)	{
		double value = scanner.nextDouble();
		while(value<min || value>max)	{
			System.out.println("Enter a value between "+ min +" and " + max + ".");
			value = scanner.nextDouble();
		}
		return value;
	}
	static void mortgageCalculator() {
		System.out.println("Mortgage Calculator");
		System.out.println("");
		System.out.println("Principal:");
		double principal = readNumber(1000,1000000);
		System.out.println("Annual Interest Rate:");
		double rate = readNumber(0,30);
		System.out.println("Years:");
		double period = readNumber(1,30);
		System.out.println("");
		System.out.println("Mortgage:" + getMortgage(principal,rate,period));
	}
	public static void main(String[] args) {
		mortgageCalculator();
	}

}


