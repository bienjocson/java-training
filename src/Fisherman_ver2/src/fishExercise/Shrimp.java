package fishExercise;

public class Shrimp extends Crustacean {
	public Shrimp() {
		super.minimumSize = 1;
		super.maximumSize = 50;
		super.name="Shrimp";
		super.size=generateSize(minimumSize,maximumSize);
	}
}
