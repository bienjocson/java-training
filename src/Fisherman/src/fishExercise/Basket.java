package fishExercise;
import java.util.LinkedList;
import java.util.Queue;

public class Basket {
	Queue<Seafood> basket= new LinkedList<Seafood>();
	int cookType;
	int removed;

	public void put(Seafood item) {
		int basketSize = basket.size();
		System.out.println("You caught a " + item.getSize() + " gram " + item.getName() + ".");
		
		if(basketSize>9)
		{
			System.out.println("Your "+ item.getSeafoodType() + " basket is full. You threw the " + item.size + " gram " + item.name + " away.");
			
		}
		else {
			basket.add(item);
		}
		
   }
	public boolean isBasketEmpty() {
		if(basket.isEmpty())
			return true;
		else
			return false;
	}
	public Seafood get() {
		if(basket.isEmpty()) {
			Seafood emptyBasket = new Seafood();
			return emptyBasket;
			
		}
		else {
			Seafood removedItem = basket.remove();
			return removedItem;
		}
		
   }
	public void cook()	{
		Seafood itemFromBasket = get();
		if(itemFromBasket.name==null) {
			System.out.println("The basket is empty.");
		}
		else	{
			//System.out.println(itemFromBasket.name);
			cookType=Fisherman.getRandomNumber(0,4);
			itemFromBasket.cook(cookType);
		}
	}
}
