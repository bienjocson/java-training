package javaTraining;
import java.util.Scanner; 
import java.lang.Math; 

public class Exercise02 {
	static Scanner scanner = new Scanner(System.in);
	static double getMortgage(double principal, double rate, int period) {
		double monthlyRate = (rate/100)/12;
		int numberOfPayments = period*12;
		return principal*
				((monthlyRate*Math.pow(1+monthlyRate,numberOfPayments))
				/(Math.pow(1+monthlyRate,numberOfPayments)-1));
	}
	static void mortgageCalculator() {
		System.out.println("Mortgage Calculator");
		System.out.println("");
		System.out.println("Principal:");
		double prinicipal = scanner.nextDouble();
		System.out.println("Annual Interest Rate:");
		double rate = scanner.nextDouble();
		System.out.println("Years:");
		int period = scanner.nextInt();
		
		System.out.println("");
		System.out.println("Mortgage:" + getMortgage(prinicipal,rate,period));
	}
	public static void main(String[] args) {
		mortgageCalculator();
	}

}


