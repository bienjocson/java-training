package fishExercise;
import java.util.Scanner; 

public class Fisherman {
	static Scanner scanner = new Scanner(System.in);
	
	final static int minimumIndexFishType=0;
	final static int maximumIndexFishType=5;
	final static int[][] seafoodSizeRange= {{1,51,201},{50,200,500}};
	
	static Basket fishBasket = new Basket();
	static Basket crustaceanBasket = new Basket();
	
	public static String[] seafoodName= {"Anchovy","Tilapia","Tuna","Shrimp","Crab","Lobster"};
	public static String[] cookTypeName= {"Corned", "Fried", "Grilled","Boiled", "Steamed"};
	
	public static int getRandomNumber(int min, int max) {
	    return (int) ((Math.random() * (max - min)) + min);
	}
	
	public static void catchSeafood()	{
		int seafoodType=getRandomNumber(minimumIndexFishType,maximumIndexFishType);
		int size=getRandomNumber(seafoodSizeRange[0][seafoodType%3],seafoodSizeRange[1][seafoodType%3]);
		
		Seafood newSeafood = new Seafood();
		newSeafood.size=size;
		newSeafood.name=seafoodName[seafoodType];
		
		System.out.println();
		
		if(seafoodType>=0 && seafoodType<=2)
			fishBasket.put(newSeafood);	
		else if(seafoodType>=3 && seafoodType<=5)
			crustaceanBasket.put(newSeafood);
	}
	
	public static void eatFish() {
		fishBasket.cook();
	}
	
	public static void eatCrustacean() {
		crustaceanBasket.cook();
	}
	public static void getBasketContent(Basket checkBasket) {
		for(Object item : checkBasket.basket){
		    System.out.println(((Seafood) item).getName().toString() + "(" + ((Seafood) item).getSize() + " grams)");
		}
		System.out.println();
	}
	public static void printBasketContent(Basket basketPrint,String basketType) {
		System.out.println();
		if(basketPrint.isBasketEmpty())
		{
			System.out.println("Your bag for " + basketType + " is empty!");
		}
		else {
		System.out.println("Here are the items in your " + basketType + " Basket!");
		getBasketContent(basketPrint);
		System.out.println("There are a total of " + basketPrint.basket.size() + " " + basketType +" in your basket.");
		}
	}
	public static void checkBaskets() {
		System.out.println();
		if(crustaceanBasket.isBasketEmpty() && fishBasket.isBasketEmpty())
		{
			System.out.println("Both baskets are empty.");
		}
		else {
			printBasketContent(fishBasket,"Fish");
			printBasketContent(crustaceanBasket,"Crustaceans");
		}
	}
	public static void prompt() {
		boolean quit = false;
		int choice;
		do {
			System.out.println("1 - Cast to catch a fish.");
			System.out.println("2 - Eat a Fish.");
			System.out.println("3 - Eat a Crustacean.");
			System.out.println("4 - Check your baskets.");
			System.out.println("5 - Quit fishing.");
			System.out.println("Enter command:");
			
			choice = scanner.nextInt();
			
			switch(choice) {
				case 1:
					catchSeafood();
					  break;
				case 2:
					eatFish();
					  break;
				case 3:
					eatCrustacean();
					  break;
				case 4:
					checkBaskets();
					  break;
				case 5:
					System.out.println("Good fishing! Let's try again next time.");
					quit=true;
					break;
				default:
					System.out.println("That is not a valid command. Try again!");
					  break;
				
			}
			System.out.println();
		}
		while(!quit);
	}


}
