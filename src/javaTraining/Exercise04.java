package javaTraining;
import java.util.Scanner; 
import java.lang.Math; 

public class Exercise04 {
	static Scanner scanner = new Scanner(System.in);
	static double getMortgage(double principal, double rate, int period) {
		double monthlyRate = (rate/100)/12;
		int numberOfPayments = period*12;
		return principal*
				((monthlyRate*Math.pow(1+monthlyRate,numberOfPayments))
				/(Math.pow(1+monthlyRate,numberOfPayments)-1));
	}
	static void mortgageCalculator() {
		System.out.println("Mortgage Calculator");
		System.out.println("");
		System.out.println("Principal:");
		double principal = scanner.nextDouble();
		while(principal<1000 || principal>1000000)	{
			System.out.println("Enter a number between 1,000 and 1,000,000.:");
			principal = scanner.nextDouble();
		}
		System.out.println("Annual Interest Rate:");
		double rate = scanner.nextDouble();
		while(rate<=0 || rate>30)	{
			System.out.println("Enter a value greater than 0 or less than or equal to 30.:");
			rate = scanner.nextDouble();
		}
		
		System.out.println("Years:");
		int period = scanner.nextInt();
		while(period<=0 || period>30)	{
			System.out.println("Enter a value between than 1 and 30.:");
			period = scanner.nextInt();
		}
		
		System.out.println("");
		System.out.println("Mortgage:" + getMortgage(principal,rate,period));
	}
	public static void main(String[] args) {
		mortgageCalculator();
	}

}


