package fishExercise;

public class Tuna extends Fish {
	public Tuna() {
		super.minimumSize = 201;
		super.maximumSize = 500;
		super.name="Crab";
		super.size=generateSize(minimumSize,maximumSize);
	}
}
