package fishExercise;

public class Fisherman {	
	final static int minimumIndexFishType=0;
	final static int maximumIndexFishType=5;
	
	static Basket fishBasket = new Basket();
	static Basket crustaceanBasket = new Basket();
	public static int generateRandomFish()	{
		int seafoodType=Main.getRandomNumber(minimumIndexFishType,maximumIndexFishType);
		return seafoodType;
	}
	public static void put(Seafood seafood) {
		if(seafood.getSeafoodType()=="Fish")
			fishBasket.put(seafood);	
		else if(seafood.getSeafoodType()=="Crustacean")
			crustaceanBasket.put(seafood);
	}
	public static void catchSeafood()	{
		int seafoodType = generateRandomFish();
		Seafood newSeafood = new Seafood();
		switch(seafoodType)	{
		case 0:
			newSeafood = new Anchovy();
			break;
		case 1:
			newSeafood = new Tilapia();
			break;
		case 2:
			newSeafood = new Tuna();
			break;
		case 3:
			newSeafood = new Shrimp();
			break;
		case 4:
			newSeafood = new Crab();
			break;
		case 5:
			newSeafood = new Lobster();
			break;
		}
		
		System.out.println();
		
		if(newSeafood.seafoodType=="Fish")
			fishBasket.put(newSeafood);	
		else
			crustaceanBasket.put(newSeafood);
	}
	
	public static void eatFish() {
		fishBasket.cook();
	}
	
	public static void eatCrustacean() {
		crustaceanBasket.cook();
	}
	public static void getBasketContent(Basket checkBasket) {
		for(Object item : checkBasket.basket){
		    System.out.println(((Seafood) item).getName().toString() + "(" + ((Seafood) item).getSize() + " grams)");
		}
		System.out.println();
	}
	public static void printBasketContent(Basket basketPrint,String basketType) {
		System.out.println();
		if(basketPrint.isBasketEmpty())
		{
			System.out.println("Your bag for " + basketType + " is empty!");
		}
		else {
		System.out.println("Here are the items in your " + basketType + " Basket!");
		getBasketContent(basketPrint);
		System.out.println("There are a total of " + basketPrint.basket.size() + " " + basketType +" in your basket.");
		}
	}
	public static void checkBaskets() {
		System.out.println();
		if(crustaceanBasket.isBasketEmpty() && fishBasket.isBasketEmpty())
		{
			System.out.println("Both baskets are empty.");
		}
		else {
			printBasketContent(fishBasket,"Fish");
			printBasketContent(crustaceanBasket,"Crustaceans");
		}
	}
}
